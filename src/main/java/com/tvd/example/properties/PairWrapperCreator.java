package com.tvd.example.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map.Entry;
import java.util.Properties;

public class PairWrapperCreator {

	private PairWrapperBuilder builder;

	public PairWrapperCreator() {
	}

	public PairWrapperCreator(PairWrapperBuilder builder) {
		setBuilder(builder);
	}

	public void setBuilder(PairWrapperBuilder builder) {
		this.builder = builder;
	}

	public void parse(String propertiesFile) {
		Properties prop = new Properties();

		// load a properties file
		try {
			File file = new File(propertiesFile);
			InputStream inputStream = null;
			if(file.exists()) {
				inputStream = new FileInputStream(file);
			}
			else {
				inputStream = getClass()
						.getClassLoader()
						.getResourceAsStream(propertiesFile);
			}
			prop.load(inputStream);
			for(Entry<Object, Object> entry : prop.entrySet()) {
				builder.addPair(entry.getKey(), entry.getValue());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
