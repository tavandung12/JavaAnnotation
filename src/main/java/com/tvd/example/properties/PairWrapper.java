package com.tvd.example.properties;

import java.util.List;
import java.util.Properties;

public interface PairWrapper {

	public List<Pair> getPairs();
	
	public void setPairs(List<Pair> pairs);
	
	public Pair getPair(int index);
	
	public Properties toProperties();
	
}
