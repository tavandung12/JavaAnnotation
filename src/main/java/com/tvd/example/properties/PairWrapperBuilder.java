package com.tvd.example.properties;

public interface PairWrapperBuilder {

	public void addPair(Object key, Object value);
	
	public void addPair(Pair pair);
	
}
