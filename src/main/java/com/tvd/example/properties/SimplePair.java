package com.tvd.example.properties;

import java.util.AbstractMap;
import java.util.Map.Entry;

public class SimplePair implements Pair {

	private Object key;
	
	private Object value;
	
	public SimplePair() {}
	
	public SimplePair(Object key, Object value) {
		setKey(key);
		setValue(value);
	}

	public Object getKey() {
		return key;
	}

	public void setKey(Object key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	@Override
	public Entry<Object, Object> toEntry() {
		Entry<Object, Object> entry = 
				new AbstractMap.SimpleEntry<Object, Object>(key, value);
		
		return entry;
	}
	
}
