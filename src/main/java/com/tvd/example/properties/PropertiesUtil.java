package com.tvd.example.properties;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesUtil {

	private PropertiesUtil() {}
	
	public static void store(Properties properties, String fileName) {
		OutputStream output = null;

		try {

			output = new FileOutputStream(fileName);

			properties.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
}
