package com.tvd.example.properties;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SimplePairWrapper implements PairWrapper {
	
	private List<Pair> pairs;
	
	public SimplePairWrapper() { 
		this(new ArrayList<Pair>());
	}
	
	public SimplePairWrapper(List<Pair> pairs) {
		setPairs(pairs);
	}

	@Override
	public List<Pair> getPairs() {
		return pairs;
	}

	@Override
	public void setPairs(List<Pair> pairs) {
		this.pairs = pairs;
	}
	
	@Override
	public Pair getPair(int index) {
		return this.pairs.get(index);
	};

	@Override
	public Properties toProperties() {
		if(pairs == null) {
			return null;
		}
		
		Properties properties = new Properties();
		for(Pair keyValue : pairs) {
			properties.setProperty(keyValue.getKey().toString(), 
					keyValue.getValue().toString());
		}
		
		return properties;
	}

	public static class Builder implements PairWrapperBuilder {
		
		private PairWrapper wrapper
			= new SimplePairWrapper();

		@Override
		public void addPair(Object key, Object value) {
			wrapper.getPairs().add(new SimplePair(key, value));
		}

		@Override
		public void addPair(Pair pair) {
			wrapper.getPairs().add(pair);
		}
		
		public PairWrapper getResult() { //get result
			return wrapper;
		}
		
	}
	
}
