package com.tvd.example.properties;

import java.util.Map.Entry;

public interface Pair {
	
	public void setKey(Object key);
	
	public void setValue(Object value);
	
	public Object getKey();
	
	public Object getValue();
	
	public Entry<Object, Object> toEntry();
	
}
