package com.tvd.example.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This Annotation indicate that entire properties of the class will parse by
 * Pdf Table Annotation Parser
 * 
 * @author tavandung12
 * @website http://www.tvd12.com
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface PdfTable {

}