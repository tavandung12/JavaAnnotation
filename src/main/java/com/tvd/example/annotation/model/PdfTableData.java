package com.tvd.example.annotation.model;

import java.util.ArrayList;
import java.util.List;

import com.tvd.example.annotation.exception.AnnotationException;
import com.tvd.example.annotation.parser.Util;

import lombok.Data;
import static com.tvd.example.annotation.parser.PdfTableAnnotationParser.*;

@Data
public class PdfTableData {

	private PdfTableStructure structure;

	private List<PdfRowData> rows;

	public PdfTableData() {
		setRows(new ArrayList<PdfRowData>());
	}

	public <T> PdfTableData(Class<T> clazz, List<T> objects) {
		this();
		try {
			setStructure(parse(clazz));
			parseObjets(objects);
		} catch (AnnotationException e) {
			e.printStackTrace();
		}
	}

	public void addRow(PdfRowData row) {
		rows.add(row);
	}

	public PdfRowData getRow(int index) {
		if (index < rows.size()) {
			return rows.get(index);
		}

		return null;
	}

	public int numberOfRows() {
		return getRows().size();
	}

	public int numberOfColumns() {
		return getStructure().getColumns().size();
	}

	private <T> PdfRowData parseObject(T object) throws AnnotationException {
		PdfRowData row = new PdfRowData();
		for (PdfColumnStructure column : structure.getColumns()) {
			row.addCell(new PdfCellData(Util.getFieldValue(object, column.getName()).toString()));
		}

		return row;
	}

	private <T> void parseObjets(List<T> objects) throws AnnotationException {
		for (T obj : objects) {
			addRow(parseObject(obj));
		}
	}
}