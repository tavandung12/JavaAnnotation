package com.tvd.example.annotation.model;

import lombok.Data;

/**
 * 
 * @author DungTV14
 *
 */

@Data
public class PdfColumnStructure {

	public PdfColumnStructure() {
	}

	public PdfColumnStructure(String name) {
		setName(name);
	}

	public String getTitle() {
		return getName();
	}

	private String name;

}