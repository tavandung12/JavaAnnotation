package com.tvd.example.annotation.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class PdfRowData {

	private List<PdfCellData> cells;

	public PdfRowData() {
		this(new ArrayList<PdfCellData>());
	}

	public PdfRowData(List<PdfCellData> cells) {
		setCells(cells);
	}

	public void addCell(PdfCellData cell) {
		cells.add(cell);
	}

	public PdfCellData getCell(int index) {
		if (index < cells.size()) {
			return cells.get(index);
		}

		return null;
	}

	public String[] getCellValues() {
		String[] result = new String[cells.size()];
		for (int i = 0; i < cells.size(); i++) {
			result[i] = cells.get(i).getValue();
		}

		return result;
	}
}