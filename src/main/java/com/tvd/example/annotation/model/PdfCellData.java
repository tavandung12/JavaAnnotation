package com.tvd.example.annotation.model;

import lombok.Data;

@Data
public class PdfCellData {

	private String value;

	public PdfCellData() {
	}

	public PdfCellData(String value) {
		setValue(value);
	}
}