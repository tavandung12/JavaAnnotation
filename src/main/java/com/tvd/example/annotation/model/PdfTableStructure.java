package com.tvd.example.annotation.model;

import java.util.List;

import lombok.Data;

/**
 * 
 * @author DungTV14
 *
 */

@Data
public class PdfTableStructure {

	private String name;

	private List<PdfColumnStructure> columns;

	public String[] getColumnTitles() {
		String result[] = new String[columns.size()];
		for (int i = 0; i < columns.size(); i++) {
			result[i] = columns.get(i).getTitle();
		}

		return result;
	}

}