package com.tvd.example.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation notify parser parse the field and field will be in list of
 * table columns
 * 
 * @author tavandung12
 * @website http://www.tvd12.com
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface PdfColumn {

	public String value() default "";

}