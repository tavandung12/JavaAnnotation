package com.tvd.example.annotation.parser;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

import com.tvd.example.annotation.NonRequestParam;
import com.tvd.example.annotation.RequestParam;
import com.tvd.example.annotation.RequestParamWrapper;
import com.tvd.example.annotation.exception.AnnotationException;

import static com.tvd.example.annotation.parser.Util.*;

/**
 * 
 * @author tavandung12
 * @website http://www.tvd12.com
 */
public class RequestParamAnnotationParser {

	/**
	 * Prevent new instance
	 */
	private RequestParamAnnotationParser() {
	}

	/**
	 * Parse class to get list of parameters
	 * 
	 * @param object
	 * @return
	 * @throws AnnotationException 
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static List<Entry<String, Object>> parse(Object object) 
			throws AnnotationException {

		Class<?> objClass = object.getClass();

		Field[] fields = objClass.getDeclaredFields();

		boolean hasParamWrapperAnnotation = 
				objClass.getAnnotation(RequestParamWrapper.class) != null;

		List<Entry<String, Object>> params = new ArrayList<Map.Entry<String, Object>>();

		try {

			if (hasParamWrapperAnnotation) {
				parseRequestParamWrapperClass(object, fields, params);
			} else {
				parseNonRequestParamWrapperClass(object, fields, params);
			}

		} catch (IllegalArgumentException 
				| IllegalAccessException e) {
			e.printStackTrace();
		}

		return params;
	}

	/**
	 * If the class has @RequestParamWrapper annotation
	 * 
	 * @param object
	 * @param fields
	 * @param params
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws AnnotationException 
	 */
	private static void parseRequestParamWrapperClass(
			Object object, 
			Field[] fields, 
			List<Entry<String, Object>> params)
					throws IllegalArgumentException, 
					IllegalAccessException, AnnotationException {
		for (Field field : fields) {
			NonRequestParam nonRequestParam = field.getAnnotation(NonRequestParam.class);
			if (nonRequestParam != null) {
				continue;
			}

			RequestParam requestParam = field.getAnnotation(RequestParam.class);

			String paramName = null;

			if (requestParam != null) {
				paramName = requestParam.value();
			} else {
				paramName = field.getName();
			}
			params.add(new SimpleEntry<String, Object>(
					paramName, 
					getFieldValue(object, field)
					));
		}
	}

	/**
	 * If the class hasn't @RequestParamWrapper annotation
	 * 
	 * @param object
	 * @param fields
	 * @param params
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws AnnotationException 
	 */
	private static void parseNonRequestParamWrapperClass(
			Object object, 
			Field[] fields,
			List<Entry<String, Object>> params) 
					throws IllegalArgumentException, 
					IllegalAccessException, 
					AnnotationException {
		for (Field field : fields) {
			RequestParam requestParam = field.getAnnotation(RequestParam.class);

			if (requestParam != null) {

				params.add(new SimpleEntry<String, Object>(
						requestParam.value(), 
						getFieldValue(object, field)));
			}
		}
	}
}
