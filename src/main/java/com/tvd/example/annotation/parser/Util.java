package com.tvd.example.annotation.parser;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.tvd.example.annotation.exception.AnnotationException;

/**
 * 
 * @author tavandung12
 * @website http://www.tvd12.com
 */
public class Util {

	/**
	 * Prevent new a instance
	 */
	private Util() {}
	
	/**
	 * 
	 * @param object
	 * @param fieldName
	 * @return
	 * @throws AnnotationException
	 */
	public static Object getFieldValue(Object object, String fieldName) 
			throws AnnotationException {
		Field field = null;
		try {
			field = object.getClass().getDeclaredField(fieldName);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		
		return getFieldValue(object, field);
	}
	
	/**
	 * 
	 * @param object
	 * @param field
	 * @return
	 * @throws AnnotationException
	 */
	public static Object getFieldValue(Object object, Field field)
			throws AnnotationException {
		try {
			Method method = getGetterMethod(object, field);
			
			return method.invoke(object, new Object[] {});
			
		} catch (NoSuchMethodException
				| SecurityException
				| IllegalAccessException
				| IllegalArgumentException
				| InvocationTargetException e) {
//			e.printStackTrace();
			
			throw new AnnotationException("Getter method not found");
		} 
		
	}
	
	/**
	 * 
	 * @param object
	 * @param field
	 * @return
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	public static Method getGetterMethod(Object object, Field field) 
			throws NoSuchMethodException, 
			SecurityException{
		Class<?> clazz = object.getClass();
		String methodName = getGetterMethodName(field);
		Method method = clazz.getDeclaredMethod(methodName, new Class<?>[] {});
		
		return method; 
		
	}
	
	/**
	 * 
	 * @param field
	 * @return
	 */
	public static String getGetterMethodName(Field field) {
		return getGetterMethodName(field.getName());
	}
	
	/**
	 * 
	 * @param fieldName
	 * @return
	 */
	public static String getGetterMethodName(String fieldName) {
		String methodName = fieldName.substring(0, 1).toUpperCase() 
				+ fieldName.substring(1);
		
		return new StringBuilder("get").append(methodName).toString();
	}
	
}
