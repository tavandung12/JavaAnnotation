package com.tvd.example.annotation.parser;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.tvd.example.annotation.NonPdfColumn;
import com.tvd.example.annotation.PdfColumn;
import com.tvd.example.annotation.PdfTable;
import com.tvd.example.annotation.exception.AnnotationException;
import com.tvd.example.annotation.model.PdfColumnStructure;
import com.tvd.example.annotation.model.PdfTableStructure;

/**
 * 
 * @author tavandung12
 * @website http://www.tvd12.com
 */
public class PdfTableAnnotationParser {

	/**
	 * Prevent new instance
	 */
	private PdfTableAnnotationParser() {
	}

	/**
	 * Parse class to get list of parameters
	 * 
	 * @param object
	 * @return
	 * @throws AnnotationException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static <T> PdfTableStructure parse(Class<T> objClass) throws AnnotationException {

		Field[] fields = objClass.getDeclaredFields();

		boolean hasPdfTableAnnotation = objClass.getAnnotation(PdfTable.class) != null;

		List<PdfColumnStructure> columns = new ArrayList<PdfColumnStructure>();

		try {

			if (hasPdfTableAnnotation) {
				parsePdfTableClass(fields, columns);
			} else {
				parseNonTableClass(fields, columns);
			}

		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();

			return null;
		}

		if (columns.size() == 0) {
			throw new AnnotationException("@PdfTable/@PdfColumn not found");
		}

		PdfTableStructure table = new PdfTableStructure();

		table.setColumns(columns);

		return table;
	}

	/**
	 * If the class has @RequestParamWrapper annotation
	 * 
	 * @param object
	 * @param fields
	 * @param params
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws AnnotationException
	 */
	private static List<PdfColumnStructure> parsePdfTableClass(Field[] fields, List<PdfColumnStructure> columns)
			throws IllegalArgumentException, IllegalAccessException, AnnotationException {

		for (Field field : fields) {
			NonPdfColumn nonPdfColumn = field.getAnnotation(NonPdfColumn.class);
			if (nonPdfColumn != null) {
				continue;
			}

			PdfColumn pdfColumn = field.getAnnotation(PdfColumn.class);

			String columName = null;

			if (pdfColumn != null) {
				columName = pdfColumn.value();
			}
			if (columName == null || columName.length() == 0) {
				columName = field.getName();
			}
			columns.add(new PdfColumnStructure(columName));
		}

		return columns;
	}

	/**
	 * If the class hasn't @RequestParamWrapper annotation
	 * 
	 * @param object
	 * @param fields
	 * @param params
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws AnnotationException
	 */
	private static void parseNonTableClass(Field[] fields, List<PdfColumnStructure> columns)
			throws IllegalArgumentException, IllegalAccessException, AnnotationException {
		for (Field field : fields) {
			PdfColumn pdfColumn = field.getAnnotation(PdfColumn.class);

			if (pdfColumn != null) {

				String columnName = (pdfColumn.value().length() == 0) ? field.getName() : pdfColumn.value();
				columns.add(new PdfColumnStructure(columnName));

			}
		}
	}
}