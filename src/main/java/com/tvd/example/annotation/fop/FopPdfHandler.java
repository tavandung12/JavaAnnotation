package com.tvd.example.annotation.fop;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.xml.sax.SAXException;

public class FopPdfHandler {
	
	private FopPdfHandler() {}
	
	public static <T> void createPDFFile(T data, String templateFile, String outputFile) 
			throws SAXException, IOException {
		createPDFFile(getXMLSource(data), templateFile, outputFile);
	}
	

	static void createPDFFile(ByteArrayOutputStream xmlSource, 
			String templateFile, String outputFile)
					throws SAXException, IOException {
		
		File xslFile = new File(templateFile);
		URL templateUrl = null;
		
		if(xslFile.exists()) {
			templateUrl = xslFile.toURI().toURL();
		}
		else {
			templateUrl = FopPdfHandler.class.getClassLoader()
					.getResource(templateFile);
		}
		
		// creation of transform source
		StreamSource transformSource = new StreamSource(templateUrl.openStream());
		
		// create an instance of fop factory
		FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
		
		// a user agent is needed for transformation
		FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
		
		// to store output
		ByteArrayOutputStream pdfoutStream = new ByteArrayOutputStream();
		StreamSource source = new StreamSource(
				new ByteArrayInputStream(xmlSource.toByteArray()));
		try {
			TransformerFactory transfact = TransformerFactory.newInstance();

			Transformer xslfoTransformer = transfact.newTransformer(transformSource);

			// Construct fop with desired output format
			Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, 
					foUserAgent, pdfoutStream);
			
			// Resulting SAX events (the generated FO)
			// must be piped through to FOP
			Result res = new SAXResult(fop.getDefaultHandler());

			// Start XSLT transformation and FOP processing
			// everything will happen here..
			xslfoTransformer.transform(source, res);

			// if you want to save PDF file use the following code
			FileUtils.writeByteArrayToFile(new File(outputFile), 
					pdfoutStream.toByteArray());

		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	static <T> ByteArrayOutputStream getXMLSource(T data) {
		JAXBContext context;

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		try {
			context = JAXBContext.newInstance(data.getClass());
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(data, outStream);
		} catch (JAXBException e) {

			e.printStackTrace();
		}
		return outStream;

	}

}