package com.tvd.example.annotation.fop;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.tvd.example.properties.PairWrapper;
import com.tvd.example.properties.PairWrapperCreator;
import com.tvd.example.properties.SimplePairWrapper;

/**
 *
 */

/**
 * @author Debasmita.Sahoo
 *
 */
@XmlRootElement(name = "EmployeeData")
public class EmployeeData {
	public EmployeeData() { }

	@XmlElementWrapper(name = "employeeList")
	@XmlElement(name = "employee")
	private List<Employee> employeeList;

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEemployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}
	
	@XmlElement(name = "title")
	public String getTitle() {
		SimplePairWrapper.Builder builder = new SimplePairWrapper.Builder();
		
		new PairWrapperCreator(builder).parse("employees.properties");
		
		PairWrapper wrapper = builder.getResult();
		
		return wrapper.getPair(0).getValue().toString();
	}

}