package com.tvd.example.annotation.fop;

import lombok.Data;

/**
 * @author Debasmita.Sahoo
 *
 */
@Data
public class Employee {
	
	private String name;
	private String employeeId;
	private String address;
	
}