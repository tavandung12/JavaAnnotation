package com.tvd.example.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This Annotation notify to parser to ignore the field and the field will not
 * be in list of table columns
 * 
 * @author tavandung12
 * @website http://www.tvd12.com
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface NonPdfColumn {

}