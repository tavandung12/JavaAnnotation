package com.tvd.example.annotation.exception;

/**
 * 
 * @author tavandung12
 * @website http://www.tvd12.com
 */
public class AnnotationException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public AnnotationException() {
		this("");
	}
	
	/**
	 * 
	 * @param msg
	 */
	public AnnotationException(String msg) {
		super(msg);
	}
	
}
