<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.1"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:barcode="org.krysalis.barcode4j.xalan.BarcodeExt" xmlns:common="http://exslt.org/common"
	xmlns:xalan="http://xml.apache.org" exclude-result-prefixes="barcode common xalan">
	<xsl:template match="Report">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple"
					page-height="20cm" page-width="10.5cm" margin-left="0.2cm"
					margin-right="0.2cm">
					<fo:region-body margin-top="0.5cm" />
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple">
				<fo:flow flow-name="xsl-region-body">
				
					<fo:block font-family="Arial" font-size="3pt" font-weight="normal">
						
						<!-- title block -->
						<fo:block font-weight="bold" border-bottom-width="0.5pt"
							border-bottom-style="solid" border-bottom-color="gray">
							<fo:block padding-bottom="3mm">
								<fo:block text-align="center">Expense Report</fo:block>
								<fo:block text-align="center">
									Report Name :
									<xsl:value-of select="name" />
								</fo:block>
							</fo:block>
						</fo:block>

						<!-- employee block -->
						<fo:block border-bottom-width="0.5pt"
							border-bottom-style="solid" border-bottom-color="gray">

							<fo:block padding-top="2mm" padding-bottom="3mm">
								<fo:table>
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Employee Name :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="employeeName" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Employee ID :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="employeeID" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:block>
						
						<!-- report header block -->
						<fo:block border-bottom-width="0.5pt"
							border-bottom-style="solid" 
							border-bottom-color="gray">
							<fo:block padding-top="2mm" padding-bottom="3mm">
								<fo:block text-align="center" font-weight="bold">
									Report Header
								</fo:block>
								<fo:table>
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Business Purpose :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="businessPurpose" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Report ID :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="reportID" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Report Date :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="reportDate" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Approval Status :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="approvalStatus" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Payment Status :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="paymentStatus" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Currency :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="currency" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Comment :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="headerComment" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:block>
						
						<!-- entries block -->
						<fo:block border-bottom-width="0.5pt"
							border-bottom-style="solid" 
							border-bottom-color="gray">
							<fo:block padding-top="2mm" padding-bottom="3mm">
								<xsl:for-each select="./entries/entry">
									<fo:block text-align="left" font-weight="bold">
										<xsl:value-of select="expenseType" />
									</fo:block>
									<fo:table>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															Transaction Date
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															Expense Type
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															Business Purpose
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															Vendor Description
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															Payment Type
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															Amount
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															Is Billable?
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															Division
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															Department
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											
											<!-- value -->
											<fo:table-row>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="normal">
															<xsl:value-of select="transactionDate" />
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															<xsl:value-of select="expenseType" />
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															<xsl:value-of select="businessPurpose" />
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															<xsl:value-of select="vendorDescription" />
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															<xsl:value-of select="paymentType" />
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															<xsl:value-of select="amount" />
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															<xsl:value-of select="isBillable" />
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															<xsl:value-of select="devision" />
													</fo:block>
												</fo:table-cell>
												<fo:table-cell>
													<fo:block border="none" 
														text-align="center"
														font-weight="bold">
															<xsl:value-of select="department" />
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
									<fo:block border="none" 
												text-align="center"
												font-weight="bold">
										<fo:inline font-weight="bold">Comment:</fo:inline> <xsl:value-of select="comment" />
									</fo:block>
								</xsl:for-each>
							</fo:block>
						</fo:block>
						
						<!-- report summary -->
						<fo:block border-bottom-width="0.5pt"
							border-bottom-style="solid" 
							border-bottom-color="gray">
							<fo:block padding-top="2mm" padding-bottom="3mm">
								<fo:table>
									<fo:table-body>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Report Total :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="reportTotal" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Personal Expenses :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="personalExpenses" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Total Amount Claimed :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="totalAmountClaimed" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Amount Approved :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="amountApproved" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Company Disbursements</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block />
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Amount Due Employee :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="amountDueEmployee" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Amount Due Company Card :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="amountDueCompanyCard" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Total Paid By Company :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="totalPaidByCompany" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Employee Disbursements</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block />
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Amount Due Company Card From Employee :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="amountDueCompanyCardFromEmployee" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell border="none" text-align="right"
												font-weight="bold">
												<fo:block>Total Paid By Employee :</fo:block>
											</fo:table-cell>
											<fo:table-cell border="none" text-align="left"
												font-weight="normal">
												<fo:block>
													&#160;
													<xsl:value-of select="totalPaidByEmployee" />
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:block>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>