package com.tvd.example.annotation;

import org.junit.Test;

import com.tvd.example.properties.PairWrapper;
import com.tvd.example.properties.PairWrapperBuilder;
import com.tvd.example.properties.PairWrapperCreator;
import com.tvd.example.properties.SimplePairWrapper;

import static org.junit.Assert.*;

public class PropertiesTest {

	@Test
	public void testGetProperties() {
		PairWrapperBuilder builder = new SimplePairWrapper.Builder();
		PairWrapperCreator creator = new PairWrapperCreator(builder);
		creator.parse("employees.properties");
		
		PairWrapper wrapper = ((SimplePairWrapper.Builder)builder)
				.getResult();
		
		assertTrue("Must equal", wrapper.getPairs().size() == 1);
	}
	
}
