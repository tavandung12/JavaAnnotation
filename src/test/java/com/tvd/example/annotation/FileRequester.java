package com.tvd.example.annotation;

import static com.tvd.example.annotation.parser.RequestParamAnnotationParser.parse;

import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.tvd.example.annotation.exception.AnnotationException;

import lombok.Data;

@Data
@RequestParamWrapper

public class FileRequester {

	@RequestParam("file-name")
	private String name;

	private long offset;

	@NonRequestParam
	private long size;

	public static void main(String[] args) 
			throws AnnotationException {
		
		FileRequester requester = new FileRequester();
		requester.setName("Dung");
		requester.setOffset(1024);

		List<Entry<String, Object>> params
			= parse(requester);
		
		for(Entry<String, Object> param : params) {
			LOGGER.info(param.getKey() + " : " + param.getValue());
		}
	}

	@NonRequestParam
	private final static Logger LOGGER = Logger.getLogger(FileRequester.class);
}
