package com.tvd.example.annotation.fop;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.xml.sax.SAXException;

public class FopPdfGenerationTest {

	private static final String HELLO_XML
		= "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"
			+ "<root>"
			+ "<name>shyam</name>"
			+ "<friend>"
			+ "<name>Abc</name>"
			+ "<phNo>90909090909</phNo>"
			+ "</friend>"
			+ "<friend>"
			+ "<name>Xyz</name>"
			+ "<phNo>32323232323</phNo>"
			+ "</friend>"
			+ "</root>";
	
//	@Test
	public void generatePdf() 
			throws SAXException, IOException, TransformerException {
		// the XSL FO file
		File xsltfile = new File("target/test-classes/helloworld.xsl");
		
		InputStream inputStream = 
				new ByteArrayInputStream(HELLO_XML.getBytes(StandardCharsets.UTF_8));
		
		// the XML file from which we take the name
//		StreamSource source = new StreamSource(new File("target/test-classes/hello.xml"));
		StreamSource source = new StreamSource(inputStream);
		// creation of transform source
		StreamSource transformSource = new StreamSource(xsltfile);
		// create an instance of fop factory
		FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
		// a user agent is needed for transformation
		FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
		// to store output
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		Transformer xslfoTransformer;
		try {
			xslfoTransformer = getTransformer(transformSource);
			// Construct fop with desired output format
			Fop fop;
			try {
				fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, outStream);
				// Resulting SAX events (the generated FO)
				// must be piped through to FOP
				Result res = new SAXResult(fop.getDefaultHandler());

				// Start XSLT transformation and FOP processing
				try {
					// everything will happen here..
					xslfoTransformer.transform(source, res);
					// if you want to get the PDF bytes, use the following code
					// return outStream.toByteArray();

					// if you want to save PDF file use the following code
					/*
					 * File pdffile = new File("Result.pdf"); OutputStream out =
					 * new java.io.FileOutputStream(pdffile); out = new
					 * java.io.BufferedOutputStream(out); FileOutputStream str =
					 * new FileOutputStream(pdffile);
					 * str.write(outStream.toByteArray()); str.close();
					 * out.close();
					 */

					// to write the content to out put stream
					byte[] pdfBytes = outStream.toByteArray();
					
					FileUtils.writeByteArrayToFile(new File("Test3.pdf"), pdfBytes);
					
				} catch (TransformerException e) {
					throw e;
				}
			} catch (FOPException e) {
				throw e;
			}
		} catch (TransformerConfigurationException e) {
			throw e;
		} catch (TransformerFactoryConfigurationError e) {
			throw e;
		}
	}

	private Transformer getTransformer(StreamSource streamSource) {
		// setup the xslt transformer
		net.sf.saxon.TransformerFactoryImpl impl = new net.sf.saxon.TransformerFactoryImpl();

		try {
			return impl.newTransformer(streamSource);

		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

}
