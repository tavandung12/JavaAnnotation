package com.tvd.example.annotation;

import static com.tvd.example.annotation.parser.RequestParamAnnotationParser.parse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tvd.example.annotation.exception.AnnotationException;

/**
 * Unit test for simple App.
 */
@RunWith(MockitoJUnitRunner.class) 
public class AppTest  {
	
	private static final List<Entry<String, Object>>
		EXPECTED = new ArrayList<>();
	
	static {
		EXPECTED.add(new SimpleEntry<String, Object>("file-name", "tiger.png"));
		EXPECTED.add(new SimpleEntry<String, Object>("offset", 1024));
	}
    
	@Test
	public void testFile() throws AnnotationException {
		FileRequester fileRequest = new FileRequester();
		fileRequest.setName("tiger.png");
		fileRequest.setOffset(1024);
		
		List<Entry<String, Object>> params = parse(fileRequest);
		assertNotNull("Params cannot null", params);
//		assertArrayEquals(params.toArray(), EXPECTED.toArray());
		
		assertThat(params, matches(EXPECTED));
		
	}
	
	@Test(expected = AnnotationException.class)
	public void testBook() throws AnnotationException {
		Book book = new Book();
		parse(book);
	}
	
	public final Matcher<List<Entry<String, Object>>> 
		matches(final List<Entry<String, Object>> expec) {
		return new BaseMatcher<List<Entry<String, Object>>>() {

			private List<Entry<String, Object>> expected = expec;
			
			@SuppressWarnings("unchecked")
			@Override
			public boolean matches(Object item) {
				List<Entry<String, Object>> result = 
						(List<Entry<String, Object>>)item;
				if((result == null && expec != null)
						|| (result != null && expec == null) 
						|| (result.size() != expected.size())) {
					return false;
				}
				for(int i = 0 ; i < result.size() ; i++) {
					if(!result.get(i).getKey()
							.equals(expected.get(i).getKey())) {
						return false;
					}
					else if(!result.get(i).getValue()
							.equals(expected.get(i).getValue())) {
						
					}
				}
				return true;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText(expected.toString());
			}
			
		};
	}
	
}
