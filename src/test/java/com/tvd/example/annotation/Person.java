package com.tvd.example.annotation;

import lombok.Data;

@Data
@PdfTable
public class Person {

	public Person() {
	}

	public Person(String name, int age, float weight) {
		setName(name);
		setAge(age);
		setWeight(weight);
	}

	public String name;

	public int age;

	public float weight;

}