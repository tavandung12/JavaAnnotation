package com.tvd.example.annotation.pdfbox;

import static com.planbase.pdf.layoutmanager.CellStyle.Align.BOTTOM_CENTER;
import static com.planbase.pdf.layoutmanager.CellStyle.Align.BOTTOM_LEFT;
import static com.planbase.pdf.layoutmanager.CellStyle.Align.BOTTOM_RIGHT;
import static com.planbase.pdf.layoutmanager.CellStyle.Align.MIDDLE_CENTER;
import static com.planbase.pdf.layoutmanager.CellStyle.Align.MIDDLE_LEFT;
import static com.planbase.pdf.layoutmanager.CellStyle.Align.MIDDLE_RIGHT;
import static com.planbase.pdf.layoutmanager.CellStyle.Align.TOP_CENTER;
import static com.planbase.pdf.layoutmanager.CellStyle.Align.TOP_LEFT;
import static com.planbase.pdf.layoutmanager.CellStyle.Align.TOP_RIGHT;
import static com.tvd.example.annotation.pdfbox.Util.vec;
import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;
import static java.awt.Color.YELLOW;
import static java.awt.Color.decode;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.junit.Test;

import com.planbase.pdf.layoutmanager.BorderStyle;
import com.planbase.pdf.layoutmanager.Cell;
import com.planbase.pdf.layoutmanager.CellStyle;
import com.planbase.pdf.layoutmanager.LineStyle;
import com.planbase.pdf.layoutmanager.LogicalPage;
import com.planbase.pdf.layoutmanager.Padding;
import com.planbase.pdf.layoutmanager.PdfLayoutMgr;
import com.planbase.pdf.layoutmanager.TextStyle;
import com.planbase.pdf.layoutmanager.XyOffset;

public class SimplePdfLayoutMgr {

	public static void main(String... args) throws IOException, COSVisitorException {
		new SimplePdfLayoutMgr().testPdf();
	}

	@Test
	public void testPdf() throws IOException, COSVisitorException {
		PdfLayoutMgr pageMgr = PdfLayoutMgr.newRgbPageMgr();

		LogicalPage lp = pageMgr.logicalPageStart();

		final float pMargin = 40;

		// Set up some useful constants for later.
		final float tableWidth = lp.pageWidth() - (2 * pMargin);
		final float pageRMargin = pMargin + tableWidth;
		final float colWidth = tableWidth / 4f;
		final float[] colWidths = new float[] { colWidth + 10, colWidth + 10, colWidth + 10, colWidth - 30 };
		final Padding textCellPadding = Padding.of(2f);

		CellStyle pageHeadCellStyle = CellStyle.of(TOP_CENTER, null, null, null);

		TextStyle pageHeadTextStyle = TextStyle.of(PDType1Font.HELVETICA, 7f, BLACK);

		float y = lp.yPageTop();

		final TextStyle regular = TextStyle.of(PDType1Font.HELVETICA, 9.5f, BLACK);
		final CellStyle regularCell = CellStyle.of(TOP_LEFT, textCellPadding, null, BorderStyle.builder()
				.left(LineStyle.of(BLACK)).right(LineStyle.of(BLACK)).bottom(LineStyle.of(BLACK)).build());

		lp.tableBuilder(XyOffset.of(40f, lp.yPageTop())).addCellWidths(vec(120f, 120f, 120f))
				.textStyle(TextStyle.of(PDType1Font.COURIER_BOLD_OBLIQUE, 12f, YELLOW.brighter())).partBuilder()
				.cellStyle(CellStyle.of(BOTTOM_CENTER, Padding.of(2), decode("#3366cc"), BorderStyle.of(WHITE)))
				.rowBuilder().addTextCells("First", "Second", "Third").buildRow().buildPart().partBuilder()
				.cellStyle(CellStyle.of(MIDDLE_CENTER, Padding.of(2), decode("#ccffcc"), BorderStyle.of(WHITE)))
				.minRowHeight(120f).textStyle(TextStyle.of(PDType1Font.COURIER, 12f, BLACK)).rowBuilder().cellBuilder()
				.align(TOP_LEFT).add("Line 1", "Line two", "Line three").buildCell().cellBuilder().align(TOP_CENTER)
				.add("Line 1", "Line two", "Line three").buildCell().cellBuilder().align(TOP_RIGHT)
				.add("Line 1", "Line two", "Line three").buildCell().buildRow().rowBuilder().cellBuilder()
				.align(MIDDLE_LEFT).add("Line 1", "Line two", "Line three").buildCell().cellBuilder()
				.align(MIDDLE_CENTER).add("Line 1", "Line two", "Line three").buildCell().cellBuilder()
				.align(MIDDLE_RIGHT).add("Line 1", "Line two", "Line three").buildCell().buildRow().rowBuilder()
				.cellBuilder().align(BOTTOM_LEFT).add("Line 1", "Line two", "Line three").buildCell().cellBuilder()
				.align(BOTTOM_CENTER).add("Line 1", "Line two", "Line three").buildCell().cellBuilder()
				.align(BOTTOM_RIGHT).add("Line 1", "Line two", "Line three").buildCell().buildRow().buildPart()
				.buildTable();

		lp = pageMgr.logicalPageStart(LogicalPage.Orientation.PORTRAIT);

		lp.putRow(pMargin, y, Cell.of(regularCell, colWidths[0], regular, "Another row of cells"),
				Cell.of(regularCell, colWidths[1], regular, "On the second page"),
				Cell.of(regularCell, colWidths[2], regular, "Just like any other page"),
				Cell.of(regularCell, colWidths[3], regular, "That's it!"));
		lp.commit();

		final LineStyle lineStyle = LineStyle.of(BLACK, 1);

		lp = pageMgr.logicalPageStart();

		lp.putCellAsHeaderFooter(pMargin, lp.yPageTop() + 10,
				Cell.of(pageHeadCellStyle, tableWidth, pageHeadTextStyle, "Test Logical Page Four"));

		// Make a big 3-page X in a box. Notice that we code it as though it's
		// on one page, and the
		// API adds two more pages as needed. This is a great test for how
		// geometric shapes break
		// across pages.

		// top lne
		lp.putLine(pMargin, lp.yPageTop(), pageRMargin, lp.yPageTop(), lineStyle);
		// left line
		lp.putLine(pMargin, lp.yPageTop(), pMargin, -lp.yPageTop(), lineStyle);
		// 3-page-long X
		lp.putLine(pMargin, lp.yPageTop(), pageRMargin, -lp.yPageTop(), lineStyle);
		// middle line
		lp.putLine(pMargin, 0, pageRMargin, 0, lineStyle);
		lp.putLine(pageRMargin, lp.yPageTop(), pMargin, -lp.yPageTop(), lineStyle);
		// right line
		lp.putLine(pageRMargin, lp.yPageTop(), pageRMargin, -lp.yPageTop(), lineStyle);
		// bottom line
		lp.putLine(pMargin, -lp.yPageTop(), pageRMargin, -lp.yPageTop(), lineStyle);
		lp.commit();

		lp.commit();

		// We're just going to write to a file.
		OutputStream os = new FileOutputStream("test2.pdf");

		// Commit it to the output stream!
		pageMgr.save(os);
	}
}