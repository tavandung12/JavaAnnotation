package com.tvd.example.annotation.pdfbox;

import java.util.Arrays;
import java.util.List;

public class Util {

	private Util() {
	}

	/* Just a convenience abbreviation for Arrays.asList() */
	@SuppressWarnings("unchecked")
	public static <T> List<T> vec(T... ts) {
		return Arrays.asList(ts);
	}
}