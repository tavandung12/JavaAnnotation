package com.tvd.example.annotation.pdfbox;

import static com.planbase.pdf.layoutmanager.CellStyle.Align.BOTTOM_CENTER;
import static com.planbase.pdf.layoutmanager.CellStyle.Align.MIDDLE_CENTER;
import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;
import static java.awt.Color.YELLOW;
import static java.awt.Color.decode;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.junit.Test;

import com.planbase.pdf.layoutmanager.BorderStyle;
import com.planbase.pdf.layoutmanager.CellStyle;
import com.planbase.pdf.layoutmanager.LogicalPage;
import com.planbase.pdf.layoutmanager.Padding;
import com.planbase.pdf.layoutmanager.PdfLayoutMgr;
import com.planbase.pdf.layoutmanager.TablePart;
import com.planbase.pdf.layoutmanager.TableRowBuilder;
import com.planbase.pdf.layoutmanager.TextStyle;
import com.planbase.pdf.layoutmanager.XyOffset;
import com.tvd.example.annotation.Person;
import com.tvd.example.annotation.model.PdfTableData;

public class PersonPdfLayoutMgr {
	public static void main(String... args) throws IOException, COSVisitorException {
		new PersonPdfLayoutMgr().testPdf();
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> vec(T... ts) {
		return Arrays.asList(ts);
	}

	@Test
	public void testPdf() throws IOException, COSVisitorException {
		// data
		PdfTableData table = new PdfTableData(Person.class, PERSONS);

		// build pdf
		PdfLayoutMgr pageMgr = PdfLayoutMgr.newRgbPageMgr();

		LogicalPage lp = pageMgr.logicalPageStart();

		TablePart tablePart = lp.tableBuilder(XyOffset.of(40f, lp.yPageTop()))
				.addCellWidths(vec(120f, 120f, 120f))
				.textStyle(TextStyle.of(PDType1Font.COURIER_BOLD_OBLIQUE, 12f, YELLOW.brighter())).partBuilder()
				.cellStyle(CellStyle.of(BOTTOM_CENTER, Padding.of(2), decode("#3366cc"), BorderStyle.of(WHITE)))
				.rowBuilder().addTextCells(table.getStructure().getColumnTitles()).buildRow().buildPart().partBuilder()
				.cellStyle(CellStyle.of(MIDDLE_CENTER, Padding.of(2), decode("#ccffcc"), BorderStyle.of(WHITE)))
				.minRowHeight(120f)
				.textStyle(TextStyle.of(PDType1Font.COURIER, 12f, BLACK));

		for (int i = 0; i < table.numberOfColumns(); i++) {
			TableRowBuilder rowBuilder = tablePart.rowBuilder();
			for (String value : table.getRow(i).getCellValues()) {
				rowBuilder.cellBuilder().align(MIDDLE_CENTER).add(value).buildCell();
			}

			rowBuilder.buildRow();
		}

		tablePart.buildPart().buildTable();

		lp.commit();

		// We're just going to write to a file.
		OutputStream os = new FileOutputStream("test1.pdf");

		// Commit it to the output stream!
		pageMgr.save(os);
	}

	private static List<Person> PERSONS = new ArrayList<Person>();

	static {
		PERSONS.add(new Person("Ta Van Dung", 23, 62.0f));
		PERSONS.add(new Person("Luu Hai Duong", 24, 50.0f));
		PERSONS.add(new Person("Nguyen Trung Duc", 25, 46.0f));
	}
}